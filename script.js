const FILM_PATTERN = document.querySelector('.film-pattern')
const BASE_URL = 'http://www.omdbapi.com'
const KEY = 'FILMS'

class Form {
    
    constructor() {
        this.status = document.querySelector('.error')
        this.field = document.querySelector('.field .input')
        this.searchBtn = document.querySelector('.searchBtn')
        
        this.searchBtn.onclick = this.search.bind(this)
        window.onkeydown = e => e.keyCode === 13 ? this.search.bind(this)() : null
        this.status.classList.add('hidden')
    }

    getStatus() {
        return this.status
    }

    encodeParams(params) { 
        return Object.keys(params).map(key => 
            [key, params[key]].map(encodeURIComponent).join('=')
        ).join('&')
    }

    async request(requestURL) {
        const responce = await fetch(requestURL)
        const result = await responce.json()
        if (result.Response === "False") throw result
        return result
    }

    error(message) {
        results.filmsBox.innerHTML = ''
        this.status.classList.remove('hidden')
        this.status.innerHTML = message
        throw new Error(message)
    }

    async search() {
        if (!this.field.value.trim()) return
        const queryParams = {
            s: this.field.value,
            apikey: 'd5677312'
        }
        const query = this.encodeParams(queryParams)
        const requestURL = `${BASE_URL}/?${query}`
        
        try {
            const data = await this.request(requestURL)
            results.render(data.Search)
        } catch(err) {
            if (err.Error) this.error(err.Error)
            else {
                console.error(err)
                this.error('Sorry, something wents wrong :(')
            }
        }
    }
}

class Results {
    constructor() {
        this.filmsBox = document.querySelector('.results .films')
        this.initialRender()
    }

    initialRender() {
        const existing = JSON.parse(localStorage.getItem(KEY) || '{}')
        const array = []
        let i = 0
        for (let key in existing) {
            existing[key].imdbID = key
            array[i] = existing[key]
            i++
        }
        this.render(array)
    }

    render(data) {
        form.getStatus().classList.add('hidden')
        this.filmsBox.innerHTML = ''
        data.forEach(film => {
            const currentFilm = new Film({
                id: film.imdbID,
                poster: film.Poster,
                title: film.Title,
                year: film.Year,
                favourite: !!film.favourite
            })
            this.filmsBox.appendChild(currentFilm.createFilmNode())
        })
    }
}

class Film {
    constructor(descr) {
        this.id = descr.id
        this.poster = (descr.poster !== 'N/A') ? descr.poster : 'img/placeholder.png'
        this.title = (descr.title !== 'N/A') ? descr.title : 'No title'
        this.year = (descr.year !== 'N/A') ? descr.year.match(/\d{4}/)[0] : 'No year'
        this.plot = ''
        this.favourite = descr.favourite
        this.toggleFavouriteBtn = {}
    }

    createFilmNode () {
        const film = FILM_PATTERN.cloneNode(true)
        this.toggleFavouriteBtn = film.querySelector('.favourite')
        this.toggleFavouriteBtn.innerHTML = this.favourite ? 'Dislike' : 'Like'
        this.toggleFavouriteBtn.onclick = this.toggleFavourite.bind(this) 
        film.classList.remove('hidden', 'film-pattern')

        film.getElementsByTagName('img')[0].src = this.poster
        film.getElementsByTagName('h2')[0].innerHTML = this.title
        film.getElementsByTagName('h2')[0].onclick = this.open.bind(this)
        film.getElementsByTagName('p')[0].innerHTML = this.year
        return film
    }

    toggleFavourite() {
        
        const existing = JSON.parse(localStorage.getItem(KEY) || '{}')
        if (this.favourite) {
            this.favourite = false
            this.toggleFavouriteBtn.innerHTML = 'Like'
            if (!existing[this.id]) return
            delete existing[this.id]
            if (!Object.keys(existing).length) {
                localStorage.removeItem(KEY)
                return
            }
            
        } else {
            this.favourite = true
            this.toggleFavouriteBtn.innerHTML = 'Dislike'
            if (existing[this.id]) return
            existing[this.id] = {
                Poster: this.poster,
                Title: this.title,
                Year: this.year,
                favourite: true
            }
        }
        localStorage.setItem(KEY, JSON.stringify(existing)) 
    }

    async open() {
        const queryParams = {
            i: this.id,
            apikey: 'd5677312'
        }
        const query = form.encodeParams(queryParams)
        const requestURL = `${BASE_URL}/?${query}`

        try {
            let data = await form.request(requestURL)
            this.plot = (data.Plot !== 'N/A') ? data.Plot : '',
            filmMore.open(this)
        } catch(err) {
            if (err.Error) this.error(err.Error)
            else {
                this.error('Sorry, something wents wrong.')
                new Error(err)
            }
        }
    }
}



class FilmMore {
    constructor () {
        this.pattern = document.querySelector('.current-film__wr')
        this.favouriteBtn = this.pattern.querySelector('.favourite')

        this.pattern.querySelector('.back').onclick = this.close.bind(this)
    }
    
    open(film) {
        this.pattern.getElementsByTagName('img')[0].src = film.poster
        this.pattern.getElementsByTagName('h2')[0].innerHTML = film.title
        this.pattern.getElementsByTagName('p')[0].innerHTML = film.year
        this.pattern.getElementsByTagName('p')[1].innerHTML = film.plot

        document.body.classList.add('cut')
        this.pattern.classList.remove('hidden')

        this.favouriteBtn.innerHTML = film.favourite ? 'Dislike' : 'Like'
        this.favouriteBtn.onclick = () => {
            this.favouriteBtn.innerHTML = film.favourite ? 'Like' : 'Dislike'
            film.toggleFavourite.bind(film)()
        }
    }
    
    close() {
        this.pattern.classList.add('hidden')
        document.body.classList.remove('cut')
    }
}

const form = new Form()
const filmMore = new FilmMore()
const results = new Results()